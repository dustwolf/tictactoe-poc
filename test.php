<?php

require_once "modules/ai/mindstore.php";

$mindstore = new mindstore("apples");

$mindstore->create(array(
 "yellowness",
 "sweetness"
));

//add sweet yellow apple
$yellowApple = array("yellowness" => "1", "sweetness" => "1");
$mindstore->add($yellowApple);

//add sweet red apple
$mindstore->add(array("yellowness" => "0.5", "sweetness" => "1"));

//add sour green apple
$mindstore->add(array("yellowness" => "0.75", "sweetness" => "0.3"));

//add nashi
$mindstore->add(array("yellowness" => "0.8", "sweetness" => "0"));

//show what is closest to a sweet yellow apple
var_dump($mindstore->closest($yellowApple));
