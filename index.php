<?php

spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

$doc = new html("Tic tac toe PoC environment", array(
 "handheldFriendly" => True,
 "bootstrap" => True,
 "css" => "slog.css"
));

$strani = new pages($doc);

?>
<div class="body-menu-wrapper">
 <div class="body-menu">
  <div id="logo">
   <a href="<?php echo $doc->url; ?>"><img src="<?php echo $doc->url; ?>images/logo.png" alt="TTT PoC"></a>
  </div>
  <div id="flags">
   <a href="?lang=en"><img src="<?php echo $doc->url; ?>images/en.gif" alt="en"></a>
  </div>
  <?php $strani->menu(); ?>
 </div>
</div>
<?php

?> 
<div class="body-text">
<?php

$strani->content();

?></div><?php 

include "footer.php"; 

?>
