<?php

require_once "dblink.php";

class gamestate {

	/*
	CREATE TABLE `gamestate` (
	  `property` varchar(15) NOT NULL,
	  `value` varchar(255) DEFAULT NULL,
	  PRIMARY KEY (`property`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
	*/

	private $db;
	private $cache;

	function __construct() {
		$this->db = new dblink();
	}

	public function __get($property) {
		if(!isset($this->cache[$property])) {
			$tmp = $this->db->q("
				SELECT `value` FROM `gamestate`
				 WHERE `property` = '".$this->db->e($property)."'
				 LIMIT 1
			");

			if(isset($tmp[0])) {
				$this->cache[$property] = $tmp[0]["value"];
			}

		}

		if(isset($this->cache[$property])) {
			return $this->cache[$property];			
		} else {
			return null;
		}
	}

	public function __set($property, $value) {
		$this->cache[$property] = $value;
		$this->db->q("
			INSERT INTO `gamestate` (`property`, `value`)
			     VALUES ('".$this->db->e($property)."', '".$this->db->e($value)."')
			 ON DUPLICATE KEY UPDATE
			 		`value` = '".$this->db->e($value)."'
		");
	}
}
