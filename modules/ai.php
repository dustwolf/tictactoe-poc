<?php

/*
TODO...

The AI plays the game using AJAX.

The AI may act out of turn, and learn the rules.

The AI uses the same database as the game, but does not access the gamestate directly.


This file understands game logic and calls mindstore and problemsolver

*/

require_once "ai/mindstore.php";
require_once "ai/problemsolver.php";

class ai {

  private $ps;
  private $mind;
  
  private $url;

  function __construct() {
    $this->mind = new mindstore();
    $this->ps = new problemsolver($this->mind);
    
    $this->getAjaxURL();
  }
  
  private function getAjaxURL() {
    $this->url = "http".(!empty($_SERVER['HTTPS'])?"s":"")."://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
    if(isset($_GET["file"])) {
     $this->url = substr($this->url, 0, -strlen($_GET["file"]));
    }
    $this->url = substr($this->url, 0, strrpos($this->url, "/")+1);

    if(isset($_SERVER["PATH_INFO"])) {
     $validPos = strpos($this->url, basename($_SERVER["SCRIPT_NAME"]));
     $this->url = substr($this->url, 0, $validPos);
    }
  }
  
  public function setup() {
    /* 
    Setup is called just once when (re)initalizing the personality
 
    The AI experiences: 
    
    The properties of the gamestate before (current):
		  array(
			  "f1" 
			  "f2" 
			  "f3" 
			  "f4" 
			  "f5" 
			  "f6" 
			  "f7" 
			  "f8" 
			  "f9" 
			  "effect"
			  "turn"
		  );

     The chosen action (ajax call and parameter).

     The properties of the gamestate after the current action (immediate).
     
     The properties of the gamestate after the opponent's action (consequence).
    
     The properties of the gamestate at the end of the game (final).
 
    */
    
    $gamestate = array(
      "f1",
      "f2",
      "f3",
      "f4",
      "f5",
      "f6",
      "f7",
      "f8",
      "f9",
      "effect",
      "turn"
	  );
	  
	  $this->mind->create(array(
	    "current" => $gamestate,
	    "action" => array(
	      "operation",
	      "move"
	    ),
	    "immediate" => $gamestate,
	    "consequence" => $gamestate,
	    "final" => $gamestate
	  ));

  }

  public function act() {
    /*
    The AI acts using AJAX
    
    
    Actions are formulated using a problem-solver stacker:
      * find experienced action that matches current state and desired outcome (find one closest)
      * if goal not achieved, simulate gamestate and add experienced action based in that state that matches desired outcome (find one closest)
      * if desired outcome achieved within 10 itterations, count it a win, if not count it a loss
      
      * for each immediate action calculate probability of desired outcome
      * pick action with best probability
      
     If problem-solver stacker produces no output, pick a move at random.
     
     
     Always learn from the result.
    */
    
    $currentState = $this->getState();
    
    $chosenAction = $this->randomMove();
    $this->doAction($chosenAction);
    
    $immediateResult = $this->getState();
    
    $this->mind->add(array(
	    "current" => $currentState,
	    "action" => $chosenAction["action"],
	    "immediate" => $immediateResult
    ));
    
    /* FIXME: problem -- how to track an experience to fill it out with the consequence and final? */
  
  }
  
  private function randomMove() {
    $possibleMoves = array(
      array("operation" => "gameMove", "move" => "f1"),
      array("operation" => "gameMove", "move" => "f2"),
      array("operation" => "gameMove", "move" => "f3"),
      array("operation" => "gameMove", "move" => "f4"),
      array("operation" => "gameMove", "move" => "f5"),
      array("operation" => "gameMove", "move" => "f6"),
      array("operation" => "gameMove", "move" => "f7"),
      array("operation" => "gameMove", "move" => "f8"),
      array("operation" => "gameMove", "move" => "f9"),
      array("operation" => "gameReset", "move" => ""),
      array("operation" => "chatSay", "move" => "hi")
    );
    
    return array("action" => $possibleMoves[array_rand($possibleMoves)]);
  }
  
  private function getState() {
    //return result of game refresh, as an associative array
    return json_decode(
      file_get_contents($this->url."ajax.php?".http_build_query(
        array("operation" => "gameRefresh")
      )
    ), true);  
  }
  
  private function doAction($experience) {  
    $experience["action"]["player"] = "machine"; //no cheating!
  
    //do game move with $experience action
    file_get_contents($this->url."ajax.php?".http_build_query($experience["action"]));
  }
  
  public function learn($experience) {
    /*
    The AI experiences: 
    
    The properties of the gamestate before:
		  array(
			  "f1" 
			  "f2" 
			  "f3" 
			  "f4" 
			  "f5" 
			  "f6" 
			  "f7" 
			  "f8" 
			  "f9" 
			  "effect"
			  "turn"
		  );

     The chosen action (ajax call and parameter).

     The properties of the gamestate after the current action.
     
     The properties of the gamestate after the oponent's action.
    
     The properties of the gamestate at the end of the game.
     
     
     In order to be able to track this stuff we'll need some kind of stack of active experiences and a few flags to track state.
    
    */
    
    //$this->mind->add();
  
  
  }

}
