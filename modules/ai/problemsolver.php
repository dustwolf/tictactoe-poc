<?php

require_once "mindstore.php";

class problemsolver {

  /*

  The mindstore algorithm makes this too complicated. 

  For now, the problemsolver can simply use exact matches.

  Problemsolver memories are made of:
    * state
    * goal

  Each of these are associative arrays, possibly with nested values, containing both the gamestate and the victory condition. It is important that the state and the goal contain the same properties.

  The arrays may have undefined values (null?) in which case the item is considered generalized (everything matches it).

  Learning / generalization is done manually for now.

  */

  //this contains the array of memories that can be used to build solutions
  /*

  Gamestate:
  X--
  -X-
  --X

  I was thinking since string matching is used, the winning positions (horizontal, vertical, diagonal) could be concatenated, even if repeating, to make the game easier to understand for the AI.

  X-- -X- --X X-- -X- --X XXX -X-

  That is after all how humans see it.

  State contains:
  * gamestate
  * move (what changed)
  * victory condition (yes or no)

  Goal is the same thing but after the oponent's move.

  Victory condition should be set at the same time as the winning moves.
  The goal for the AI can be the victory condition alone.

  */
  private $mind;
  
  function __construct() {
    $this->mind = array(
      array(
        "state" => array("test" => "example1"),
        "goal" => array("test" => "example2")
      ),
      array(
        "state" => array("test" => "example2"),
        "goal" => array("test" => "example3")
      ),
      array(
        "state" => array("test" => "example3"),
        "goal" => array("test" => "example4")
      )
    );

  }
  
  public function solve($state, $goal) {
    /*
    A goal is an experience that is a combination of the current state plus the desired outcome
  
    Algorithm:  
      * find experienced action that matches the goal using $this->match();


      * if goal is not the immediate result, use experience state goal as current state and search again
      * if goal is the state achieved within 10 itterations, count it a win (solution found), if not count it a loss (no solution)

      (10 is more than enough as a tic-tac-toe game will always resolve after 4-5 moves depending on who starts.)

      
      * for each chosen action calculate win probability
      * return action with best probability
    k
    */

    $tmp = $this->simulate($state, $goal);
    $solution = $tmp["solution"];
    $gotSolution = $tmp["gotSolution"];

    //if no solutions are found, go for a random one
    if(!$gotSolution) {
      $solution = array_rand($this->mind);
    }

    return $this->mind[$solution]; //this could potentially return nothing if mind is empty
  
  }

  private function simulate($state, $goal, $potentialSolution = False, $gotSolution = False, $depth = 0) {
    if(!$gotSolution && $depth < 10) {

      //try each possible solution, from most matching to least
      //$match is match % and isn't used
      foreach($this->associate($goal, "goal") as $move => $match) {

        //while the system simulates everything, only the first move is relevant
        //TODO: This is wrong. The last move is relevant rather than the first
        //the first move is most similar to the solution, the last move is the one you can immediately take
        if($potentialSolution === False) {
          $potentialSolution = $move;
        }

        //the new goal is the memory state, working backwards to the current state
        $subGoal = $this->mind[$move]["state"];

        //If the simulation arrived at current state, this is a solution...
        //I wonder if imperfect solutions should be allowed, or where the limit is
        //this should probably be evaluated in terms of best fits rather than 0
        if($this->match($subGoal, $state) == 0) {

          //this only happens in the most nested simulate
          //potentially
          $gotSolution == True;
          $solution = $potentialSolution; //winning move required to know we have solution
          break; //stop considering less well matching solutions

        } else {

          //recurse into possible sub-solutions
          //not sure if parameter passing correct, TODO
          $depth++;
          $tmp = $this->simulate($state, $subGoal, $potentialSolution, $gotSolution, $depth);
          $solution = $tmp["solution"];
          $gotSolution = $tmp["gotSolution"];

        }

        $potentialSolution = False; //solution didn't work, consider next one

      }

    }

    return array(
      "solution" = $solution,
      "gotSolution" = $gotSolution
    );

  }

  private function associate($desired, $stateOrGoal = "goal") {
    $results = array();
    foreach($this->mind as $i => $memory) {
      $results[$i] = $this->match($memory[$stateOrGoal], $desired);
    }
    asort($results); //sort results from most matching to least

    //it would potentially be a good idea to limit output here by length
    //this would limit itterations to consider without affecting immagination scope
    //or I guess you could do it incrementally

    return $results;
  }

  //flat associative array imperfect string matching
  //returns 0 if perfect match or % difference if not
  //ignores properties not set or null, or not matching
  private function match($a, $b) {
    $out = 0; $percent = 0; $c = 0;
    foreach($a as $property => $value) {
      if($value !== null && isset($b[$property])) {
        similar_text((string) $value, (string) $b[$property], $percent);
        $out += (100-$percent); //percent difference
        $c++;
      }
    }
    return ($out / $c);
  }



}

