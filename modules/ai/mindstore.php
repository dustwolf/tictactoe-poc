<?php

require_once "modules/dblink.php";

class mindstore {
  /*
    TODO: This should be able to handle points without all columns
  */

  private $db;
  private $id;
  
  function __construct($id = "default") {
    $this->db = new dblink();

    //this is the mindstore ID
    $this->id = $this->db->e($id);
  }
  
  public function create($structure = array()) {
    /*
    walk $structure
    and create a database CREATE statement for storing datapoints with the same structure
    structure is a flat array of property names
    table name is $this->id to enable multiple mindstores
    */
    if(!$this->db->tableExists($this->id)) {
      $structure = array_diff($structure, array("id")); //"id" column not allowed

      $columns = array(); $primaryKey = array();
      foreach($structure as $property) {
        $prop = "`".$this->db->e($property)."`";
        $columns[] = $prop." FLOAT,";
        $primaryKey[] = $prop;
      }

      //the primary key is the coordinates, not the ID
      //the coordinates should not be duplicatable
      //the ID is only used for convenience in the index
      $this->db->q("
        CREATE TABLE `".$this->id."` (
          `id` VARCHAR(40) NOT NULL,
          ".implode($columns)."
          PRIMARY KEY (".implode($primaryKey, ", ")."));
      ");

      $this->db->q("
        CREATE TABLE `".$this->id."-index` (
          `a` VARCHAR(40) NOT NULL,
          `b` VARCHAR(40) NOT NULL,
          `distance` FLOAT NOT NULL,
          PRIMARY KEY (`a`, `b`));
      ");
    }
  }
  
  public function add($point) {
    /*
    walk $point 
    and create INSERT query
    property => value (float)
    use $this->index
    
    it is assumed the point structure matches $this->create()
    */
    #insert/ignore
    $point["id"] = $this->hash($point);
    $this->db->insert($this->id, $point, true);
    $this->index($point);

    return $point; //for internal class use
  }

  private function index($pointA) {
    /*
    walk $point 
    create/update $this->compare index: point A, point B, distance
    first column always contains the lower value point id.
    
    it is assumed the point structure matches $this->create()

    */
    $pointA = $this->find($pointA); //ensure point ID is set

    foreach($this->db->q("SELECT * FROM `".$this->id."`") as $pointB) {
      if(strcmp($pointA["id"], $pointB["id"]) < 0) {
        $idA = $pointA["id"];
        $idB = $pointB["id"];
      } else {
        $idA = $pointB["id"];
        $idB = $pointA["id"];
      }

      if($pointA["id"] != $pointB["id"]) {
        #insert ignore (points cannot move)
        $this->db->insert($this->id."-index", array(
          "a" => $idA,
          "b" => $idB,
          "distance" => $this->compare($pointA, $pointB)
        ), true);
      }
    }
  
  }
  
  public function closest($point) {
    /*
    walk $point
    return closest point from $this->index (or list sorted by distance)

    it is assumed the point structure matches $this->create()
    */

    //FIXME: This will return an ID even if the point doesn't exist
    $point = $this->find($point);

    $out = array();
    foreach($this->db->q("
      SELECT `a`,`b`,`distance` FROM `".$this->id."-index`
       WHERE `a` = '".$this->db->e($point["id"])."' 
          OR `b` = '".$this->db->e($point["id"])."'
       ORDER BY `distance` ASC
    ") as $entry) {

      //ID of matching point is the other one
      if($entry["a"] == $point["id"]) {
        $id = $entry["b"];
      } else {
        $id = $entry["a"];
      }

      //output is array of arrays because order is important
      //so [0]["id"] is always the closest
      $out[] = array(
        "point" => $this->find($id),
        "distance" => $entry["distance"]
      );
    }

    return $out;
  }

  private function find($point) {
    /*
    Find point in DB and add ["id"]
    Probably add it to the database if absent, it should not be possible to query database for a point that isn't in it
    */

    //if only ID given
    if(!is_array($point)) {
      $point = array("id" => $point);
    }

    if(!isset($point["id"])) {
      //if ID not set, find it
      $point["id"] = $this->hash($point);
    
    } else {
      //if only ID is set, fill out the rest
      if(array_keys($point) === array("id")) {
        $point = $this->db->q("
          SELECT * FROM `".$this->id."`
           WHERE `id` = '".$this->db->e($point["id"])."'
        ")[0]; //it should not be possible to have an ID of a non-existent point
      }
    }
    return $point;
  }

  private function hash($point) {
    //generate ID value from point data
    unset($point["id"]); //avoid catch 22
    return sha1(implode($point,"-"));
  }
  
  private function compare($a, $b) {
    /*
    provide sqrt of total of squares according to $this->create() structure, for calculating distance into value between 0(match) and 1(differs)
    */
    unset($a["id"]); unset($b["id"]); //IDs not used in distance calculation

    $total = (float) 0;
    foreach($a as $property => $valueA) {
      $total += pow(((float) $valueA) - ((float) $b[$property]), 2);
    }
    return (float) sqrt($total);
  }

}
