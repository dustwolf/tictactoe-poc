<?php

class image {
 
 private $path;
 private $alias;

 function __construct($path) {
  $this->path = $path;
  $this->alias = str_ireplace(array("/", " ", ".jpg", "."), "_", $this->path);
 }

 function cached($width) {
  return "images/cache/".$this->alias."_".$width.".jpg";
 }

 function resize($nW) {

  //CALCULATE DIMENSIONS
  list($oW, $oH) = getimagesize($this->path);
  $nH = $oH * ($nW / $oW);  
 
  //CREATE CONTAINER & LOAD FILE
  $thumb = imagecreatetruecolor($nW, $nH);
  $source = imagecreatefromjpeg($this->path);

  //RESIZE
  imagecopyresampled($thumb, $source, 0, 0, 0, 0, $nW, $nH, $oW, $oH);
  
  //SAVE FILE
  imagejpeg($thumb, $this->cached($nW));

 }

 function resized($width = 0) {

  if($width > 0) {
  
   //NEEDS TO BE RESIZED
   if(!file_exists($this->cached($width))) {
    $this->resize($width);
   }

   $out = $this->cached($width);

  } else {

   //JUST SEND ORIGINAL SIZE
   $out = $this->path;

  }

  return $out;

 }

}

?>
