<?php

require_once "dbconfig.php";

class dblink {

 public $link;

 function __construct($database = "", $username = "", $password = "", $hostname = "") {

  $c = new dbconfig();
  if($database == "") { $database = $c->database; }
  if($username == "") { $username = $c->username; }
  if($password == "") { $password = $c->password; }
  if($hostname == "") { $hostname = $c->hostname; }
  
  $this->link = new mysqli($hostname, $username, $password, $database);
  $this->link->set_charset("utf8");

 }
 
 public function q($q, $primary = "") {
    
  $r = $this->link->query($q);
  if ($this->link->error) {
    echo $this->link->error;
    http_response_code(500);
  }

  $results = array();
  if($r !== True && $r !== False) {
   while($tmp = $r->fetch_assoc()) {
    if($primary == "") {
     $results[] = $tmp;
    } else {
     $results[$tmp[$primary]] = $tmp;
    }
   }
   return $results;
  } else {
   return $this->link->affected_rows;
  }

 }
 
 public function e($e) {
  return $this->link->real_escape_string($e);
 }

 public function ae($polje) {
  $tmp = array();
  foreach($polje as $kljuc => $vrednost) {
   $tmp[$kljuc] = $this->e($vrednost);
  }
  return $tmp;
 }

 public function flatten($array = array(), $parameter = "") {
  if($parameter == "") {
   reset($array);
   $tmp = $array[key($array)];
   reset($tmp);
   $parameter = key($tmp);
  }

  $out = array();
  foreach($array as $vnos) {
   $out[] = $vnos[$parameter]; 
  }

  return $out;
 }
 
 public function insert($table, $data = array(), $ignore = False) {
  if($ignore) {$ignore = "IGNORE ";} else {$ignore = "";}
  return $this->q("
   INSERT ".$ignore."INTO `".$table."` (`".implode("`,`", array_keys($data))."`)
   VALUES ('".implode("','", $this->ae($data))."')
  "); 
 }

 public function tableExists($table) {
  return in_array($table, 
   $this->flatten($this->q("SHOW TABLES"))
  );
 }

 function __destruct() {
  $this->link->close();
 }

}
