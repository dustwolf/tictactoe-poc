<?php

spl_autoload_register(function ($class_name) { require_once $class_name.'.php'; });

class pages {

 public $link;
 private $pages;
 private $context;
 
 private $sources;

 function __construct($context = "") {
  $this->getPages();
  $this->getLink();
  if($context === "") {
   $this->context = new html();
  } else {
   $this->context = $context;
  }
 }

 function getLink() {
 
  //GET LINK LIKE index.php/introduction.html BECOMES introduction.html
  if(isset($_SERVER["PATH_INFO"])) {
   $this->link = substr(escapeshellcmd(urldecode($_SERVER["PATH_INFO"])),1);
  } else {
   $this->link = "";
  }

  //IF THERE IS A .html OR SUCH, REMOVE IT
  $linkEnd = strrpos($this->link,".");
  if($linkEnd > 0) {
   $this->link = substr($this->link,0,$linkEnd);
  }

  //IF NOTHING IS PROVIDED, GO WITH Home
  if($this->link == "") {
   $this->link = "1-About";
  }

 }

 function getPages() {
  $this->pages = array();
  if ($handle = opendir('pages')) {
	while (false !== ($entry = readdir($handle))) {
		if ($entry != "." && $entry != ".." && substr($entry, 0, 1) != '0') {
			$this->pages[] = $entry;
		}
	}
	closedir($handle);
  }
  sort($this->pages);  

 }

 function menu() {

	?><ul class="menu"><?php
	//FOR EACH FILE...
	foreach($this->pages as $page) {

		//REMOVE THE NUMBER IN FRONT
		$text = substr($page,strpos($page,"-")+1);

		//REPLACE DASHES WITH SPACES
		$text = str_replace("-", " ", $text);

		$language = new language();
		$text = $language->translate("pages/".$page, $text);

		//IF IT'S THE CURRENT LINK, DISPLAY IT DIFFERENTLY
		if($this->link == $page) {
			echo '<li><a class="current">'.ucfirst($text).'</a></li>';
		} else {
			echo '<li><a href="'.$this->context->url."index.php/".rawurlencode($page).'">'.ucfirst($text).'</a></li>';
		}

	}
	?></ul><?php
 }

 function content($page = "") {
	if($page == "") {
		$page = $this->link;
	}
	
	//CHECK TO SEE IF SOMEONE IS TRYING TO STEAL A FILE
	if(strpos(realpath("pages/".$page."/en.php"), realpath(dirname(__FILE__)."/../pages")) === False) {

		include "404.php";

	} else {

		//IF NOT, CHECK TO SEE IF THAT PAGE EXISTS
		if(!file_exists("pages/".$page."/en.php")) {

			//IT DOESN'T
			include "404.php";

		} else {

			//IT DOES, SPELL IT OUT
			$language = new language();

			if(file_exists("pages/".$page."/".$language->code.".php")) {
				include "pages/".$page."/".$language->code.".php";
			} else {
				include "pages/".$page."/en.php";	
			}

		}
	}  

 }
 
 private function source($url, $text = "") {
  if($text == "") {
   $text = $url;
  }
  $this->sources[] = array("url" => $url, "text" => $text);
  end($this->sources); $i = key($this->sources);
  echo '<a href="#<?php echo $i; ?>"><sup>['.($i+1).']</sup></a>';
 }
 
 private function sourceList() {
  ?><ol><?php
  foreach($this->sources as $i => $source) {
   ?><li value="<?php echo $i+1; ?>"><a id="<?php echo $i; ?>" href="<?php echo $source["url"]; ?>"><?php echo $source["text"]; ?></a></li><?php
  }
  ?></ol><?php
 }

}

