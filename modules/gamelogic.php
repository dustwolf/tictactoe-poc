<?php

class gamelogic {
	
	public function playerDisplay($state) {
		if($state === "machine") {
			return "O";
		} elseif($state === "human") {
			return "X";
		} else {
			return "";
		}
	}

	public function victoryCondition($s, $player) {
		$v = False;

		foreach(array("human", "machine") as $p) {
			if(
						($this->checkRow($s, "f1", "f2", "f3") == $p)
				||	($this->checkRow($s, "f4", "f5", "f6") == $p)
				||	($this->checkRow($s, "f7", "f8", "f9") == $p)
				||	($this->checkRow($s, "f1", "f4", "f7") == $p)
				||	($this->checkRow($s, "f2", "f2", "f8") == $p)
				||	($this->checkRow($s, "f3", "f6", "f9") == $p)
				||	($this->checkRow($s, "f1", "f5", "f9") == $p)
				||	($this->checkRow($s, "f3", "f5", "f7") == $p)
			) {
				if($p == $player) {
					return "win";
				} else {
					return "loose";
				}
			}
		}
		return "play";

	}

	private function checkRow($s, $first, $center, $last) {
		if($s->{$center} == $s->{$first} && $s->{$center} == $s->{$last}) {
			return $s->{$center};
		} else {
			return null;
		}
	}

}