<?php

class language {

 public $code;
 private $translationCache;

 function __construct() {

  $this->translationCache = array();

  if(isset($_GET["lang"])) {

   $this->code = $_GET["lang"];
   if(!headers_sent()) {
    setcookie("lang", $this->code, strtotime("+1 year"), "/");
   }

  } else {

   if(isset($_COOKIE["lang"])) {
    $this->code = $_COOKIE["lang"];
   } else {
    if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
     $lc = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
     if($lc == "sl") {
      $this->code = "sl";
     } elseif($lc == "de") {
      $this->code = "de";
     } else {
      $this->code = "en";
     }
    }
    if(!headers_sent()) {
     setcookie("lang", $this->code, strtotime("+1 year"), "/");
    }
   }

  }
 
 }

 function translate($path, $fallback) {

  $out = $fallback;
 
  if(isset($this->translationCache[$path])) {
   $out = $this->translationCache[$path];
  } else {

   //LOAD TRANSLATION IF IT EXISTS
   if(file_exists($path."/info.php")) {
    include $path."/info.php";
    if(isset($translation[$this->code])) {
     $out = $translation[$this->code];				
    }
   }  

   $this->translationCache[$path] = $out;

  }

  return $out;

 }

}

?>
