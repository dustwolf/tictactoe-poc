<h1>Game</h1>

<?php
$this->context->addJS(
	"settings", 
	"window.ajaxurl=".json_encode($this->context->url."ajax.php").";"
);
$this->context->loadJSFile("game.js");
?>

<table id="game">
	<tbody>
		<tr>
			<td id="f1"></td>
			<td id="f2"></td>
			<td id="f3"></td>
		</tr>
		<tr>
			<td id="f4"></td>
			<td id="f5"></td>
			<td id="f6"></td>
		</tr>
		<tr>
			<td id="f7"></td>
			<td id="f8"></td>
			<td id="f9"></td>
		</tr>
	</tbody>
</table>
<br>
<div id="gameButtons">
	<button id="gameReset" class="btn btn-danger">Reset game</button>
	Effect of last move: <span id="effect"></span>. 
	It's <span id="turn"></span>'s turn.
</div>

<div id="chat">
	<div id="chat_inbox">
		Machine player says: <span id="chat_machine"></span><br>
		Human player says: <span id="chat_human"></span><br>
	</div>
	<input type="text" id="chat_say">
</div>