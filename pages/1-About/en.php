<h1>Some history</h1>

<p>Forgive me for rambling a bit before I have actually put down any code.</p>

<p>This project is based on something we were working on with a friend a very long time ago (I think back when HL2 was still an exciting new thing). My friend wanted to make a bot that could learn a natural language and though he did not succeed, he did create a bot that appeared to be capable of comprehending true meaning.</p>

<p>Of course programs do not have eyes and ears and do not percieve their environments as we do (and I'm not good enough in electronics to build robots), so this could never amount to anything any one of you could recognize as true understanding. However the potential was there and I was captivated by this idea ever since.</p>

<p>One of the bariers to overcome was how to use this true understanding for anything useful. Recently I had begun a thought experiment, of creating an AI capable of independent problem-solving. I've come up with a few ways in which this could be done, based on work previously done on the aforementioned idea.</p>

<p>It is now time to create an actual example of a generic learning, problem-solving AI, to see if it works. For this purpose I have opted for a trivial test environment -- the AI is to learn to play the game Tic tac toe (in honour of movies even I should be too young to be familiar with).</p>

<p>I am doing this in my free time and feel rather taxed by my day job, so forgive me if this does not yield anything useful for a while. It is however a project I am motivated to complete.</p>

<span style="text-align: center; display: block;">~&nbsp;·&nbsp;~</span><br>

<p>I owe you a few words about the AI planned to be tested with this environment.</p>

<p>The AI does not know how to play the game. The AI does not know how to talk. The idea is that it learns. The chat function I plan to integrate into this environment is going to be tied to the same memory matrix as the game, therefore while the AI does not understand the sentences as you or I might, it does have a sense how they might be related to the game state. Therefore it should be able to learn to say GG.</p>

<p>The initial AI probably has no concept of symetry or symbols being in a row or anything like that. Given the minimal scope of the possible moves in the game, this should not be a great issue.</p>

<p>The AI never plays the game against itself and only learns from playing the game with you (and other human players). It sees your move as a consequence of it's actions, and is attempting to plan it's own actions to resolve the problem.</p>

<p>The AI is not necesarily playing to win. You will be able to configure these parameters on the Setup page. I am hoping the AI will learn how to be a good sport.</p>
