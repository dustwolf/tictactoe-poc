<?php

spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

header('Content-Type: application/json');
$out = array(); ob_start();

if(isset($_REQUEST["operation"])) {

	$s = new gamestate();
	$l = new gamelogic();

	if($_REQUEST["operation"] == "gameMove") {

		//if field not taken
		if($s->{$_REQUEST["move"]} != "human" 
		&& $s->{$_REQUEST["move"]} != "machine") {

			//if your turn
			if($s->turn == $_REQUEST["player"]) {

				//if game is not already lost or won
				if($l->victoryCondition($s, $_REQUEST["player"]) == "play") {
					$s->{$_REQUEST["move"]} = $_REQUEST["player"];
					$s->{$_REQUEST["player"]."_effect"} = 
						$l->victoryCondition($s, $_REQUEST["player"]);

					//change turn
					if($s->turn == "human") {
						$s->turn = "machine";
					} else {
						$s->turn = "human";
					}

				} else {
					$s->{$_REQUEST["player"]."_effect"} = "invalidMove";
				}

			} else {
				$s->{$_REQUEST["player"]."_effect"} = "invalidMove";
			}

		} else {
			$s->{$_REQUEST["player"]."_effect"} = "invalidMove";
		}

	} elseif($_REQUEST["operation"] == "gameReset") {

		$s->{$_REQUEST["player"]."_effect"} = "reset";
		$s->turn = "human";
		$s->f1 = null;
		$s->f2 = null;
		$s->f3 = null;
		$s->f4 = null;
		$s->f5 = null;
		$s->f6 = null;
		$s->f7 = null;
		$s->f8 = null;
		$s->f9 = null;
		
	} elseif($_REQUEST["operation"] == "gameRefresh") {

		$out = array(
			"f1" => $l->playerDisplay($s->f1),
			"f2" => $l->playerDisplay($s->f2),
			"f3" => $l->playerDisplay($s->f3),
			"f4" => $l->playerDisplay($s->f4),
			"f5" => $l->playerDisplay($s->f5),
			"f6" => $l->playerDisplay($s->f6),
			"f7" => $l->playerDisplay($s->f7),
			"f8" => $l->playerDisplay($s->f8),
			"f9" => $l->playerDisplay($s->f9),
			"chat_machine" => $s->chat_machine,
			"chat_human" => $s->chat_human,
			"effect" => $s->human_effect,
			"turn" => $s->turn
		);

	} elseif($_REQUEST["operation"] == "chatSay") {
		$s->{"chat_".$_REQUEST["player"]} = $_REQUEST["text"];
		$s->{$_REQUEST["player"]."_effect"} = "chat";
		
		//in case of AI learn... text = move for consistency
	}
}



//output valid JSON
echo json_encode(($out + array("debug" => ob_get_clean())));
