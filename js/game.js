$('#game td').click(function(){

	$.ajax({
	  url: window.ajaxurl,
	  data: {
	  	operation: "gameMove",
	  	player: "human",
	  	move: $(this).attr("id")
	  },
	  cache: false,
	  success: function(result){
	  	console.log(result);
	  	window.gameRefresh();
	  }
	});
});

$('#gameReset').click(function(){

	$.ajax({
	  url: window.ajaxurl,
	  data: {
	  	operation: "gameReset",
	  	player: "human"
	  },
	  cache: false,
	  success: function(result){
	  	console.log(result);
	  	window.gameRefresh();
	  }
	});
});

$('#chat #chat_say').bind('keypress', function(e) {
	if(e.keyCode==13){
		$.ajax({
		  url: window.ajaxurl,
		  data: {
		  	operation: "chatSay",
		  	player: "human",
		  	text: $(this).val()
		  },
		  cache: false,
		  success: function(result){
		  	console.log(result);
		    window.gameRefresh();
		  }
		});
		$(this).val("");
	}
});

$(document).ready(function() {
	window.gameRefresh();
});

window.gameRefresh = function() {
	$.ajax({
	  url: window.ajaxurl,
	  data: {
	  	operation: "gameRefresh"
	  },
	  cache: false,
	  success: function(result){
	  	console.log(result.effect);
	    $("#game #f1").html(result.f1);
	    $("#game #f2").html(result.f2);
	    $("#game #f3").html(result.f3);
	    $("#game #f4").html(result.f4);
	    $("#game #f5").html(result.f5);
	    $("#game #f6").html(result.f6);
	    $("#game #f7").html(result.f7);
	    $("#game #f8").html(result.f8);
 	    $("#game #f9").html(result.f9);
 	    $("#chat #chat_human").html(result.chat_human);
 	    $("#chat #chat_machine").html(result.chat_machine);
 	    $("#effect").html(result.effect);
 	    $("#turn").html(result.turn);
	  }
	});	
}